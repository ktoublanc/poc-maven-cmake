#include "CUnit/Basic.h"
#include <unistd.h>
#include <sys/wait.h>
#include <limits.h>


void test_main(void) {
    pid_t pid = fork();
    if (pid > 0) {
        int result;
        wait(&result);
        CU_ASSERT_EQUAL(result, 0);
    } else {
        char *args[] = {
                "Child forked execve",
                "1",
                NULL
        };
        if (execv("SimpleApplication", args) < 0) {
            perror("Unable to exec program");
            CU_FAIL("Unable to exec program");
        }
    }
}
