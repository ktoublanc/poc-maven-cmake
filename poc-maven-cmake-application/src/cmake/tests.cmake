ENABLE_TESTING()

function(CREATE_TEST_TARGETS)
    SET(options "")
    SET(oneValueArgs TEST_TARGET)
    SET(multiValueArgs TEST_SOURCES INCLUDE_DIRECTORIES LINK_LIBRARIES DEPENDENCIES)

    CMAKE_PARSE_ARGUMENTS(ARGUMENTS "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    FIND_PATH(CUNIT_INCLUDES CUnit/Basic.h
            HINTS "/usr/local/Cellar/cunit/2.1-3/include/")

    FIND_LIBRARY(CUNIT_LIBRARY
            NAMES cunit libcunit.a
            HINTS "/usr/local/Cellar/cunit/2.1-3/lib/")

    foreach (CUNIT_TEST_SUITE ${ARGUMENTS_TEST_SOURCES})
        # Reset Test Cases
        SET(CUNIT_TEST_CASES "")

        # Get test suite name
        GET_FILENAME_COMPONENT(CUNIT_TEST_SUITE_NAME ${CUNIT_TEST_SUITE} NAME_WE)
        MESSAGE("-- Processing test suite: ${CUNIT_TEST_SUITE_NAME}")

        # Search for test cases
        FILE(STRINGS ${CUNIT_TEST_SUITE} CUNIT_TEST_SUITE_TEST_FUNCTIONS REGEX "^void +test_.*\\((void)?\\) *{?$")
        foreach (CUNIT_TEST_SUITE_TEST_FUNCTION ${CUNIT_TEST_SUITE_TEST_FUNCTIONS})
            STRING(REGEX REPLACE "^.*(test_.*)\\(.*$" "\\1" CUNIT_TEST_SUITE_TEST_CASE ${CUNIT_TEST_SUITE_TEST_FUNCTION})
            LIST(APPEND CUNIT_TEST_CASES ${CUNIT_TEST_SUITE_TEST_CASE})
        endforeach (CUNIT_TEST_SUITE_TEST_FUNCTION)
        MESSAGE("-- Found test cases: ${CUNIT_TEST_CASES}")


        # Create prototypes
        LIST(JOIN CUNIT_TEST_CASES "(void); void " CUNIT_TEST_SUITE_PROTOTYPES)
        SET(CUNIT_TEST_SUITE_PROTOTYPES "void ${CUNIT_TEST_SUITE_PROTOTYPES}(void);")

        # Create test suite array variable
        LIST(JOIN CUNIT_TEST_CASES "," CUNIT_TEST_SUITE_ARRAY)

        # Create test name variable
        LIST(JOIN CUNIT_TEST_CASES "\", \"" CUNIT_TEST_SUITE_ARRAY_NAMES)
        SET(CUNIT_TEST_SUITE_ARRAY_NAMES "\"${CUNIT_TEST_SUITE_ARRAY_NAMES}\"")

        # Configure test launcher
        CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/src/cmake/launcher.c.in launch_${CUNIT_TEST_SUITE_NAME}.c)

        #Create test executable
        ADD_EXECUTABLE(${CUNIT_TEST_SUITE_NAME} ${CUNIT_TEST_SUITE} launch_${CUNIT_TEST_SUITE_NAME}.c)

        # Configure includes
        TARGET_INCLUDE_DIRECTORIES(${CUNIT_TEST_SUITE_NAME} PUBLIC ${CUNIT_INCLUDES})
        if (ARGUMENTS_INCLUDE_DIRECTORIES)
            TARGET_INCLUDE_DIRECTORIES(${CUNIT_TEST_SUITE_NAME} PUBLIC ${ARGUMENTS_INCLUDE_DIRECTORIES})
        endif ()

        # Configrure libraries
        TARGET_LINK_LIBRARIES(${CUNIT_TEST_SUITE_NAME} ${CUNIT_LIBRARY})
        if (ARGUMENTS_LINK_LIBRARIES)
            TARGET_LINK_LIBRARIES(${CUNIT_TEST_SUITE_NAME} ${ARGUMENTS_LINK_LIBRARIES})
        endif ()

        # Configure Deps
        if (ARGUMENTS_DEPENDENCIES)
            ADD_DEPENDENCIES(${CUNIT_TEST_SUITE_NAME} ${ARGUMENTS_DEPENDENCIES})
        endif ()

        ADD_TEST(${CUNIT_TEST_SUITE_NAME} ${CUNIT_TEST_SUITE_NAME})
    endforeach (CUNIT_TEST_SUITE)
endfunction()
